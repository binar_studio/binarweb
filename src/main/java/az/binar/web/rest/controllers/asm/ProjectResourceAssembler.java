package az.binar.web.rest.controllers.asm;

import az.binar.web.core.model.Project;
import az.binar.web.rest.controllers.ProjectController;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import java.util.ArrayList;
import java.util.List;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

/**
 * Created by togrul on 12/5/14.
 */
public class ProjectResourceAssembler extends ResourceAssemblerSupport<Project, Resource> {

    public ProjectResourceAssembler() {
        super(ProjectController.class, Resource.class);
    }

    public ProjectResourceAssembler getProjectResourceAssesmber() {
        return new ProjectResourceAssembler();
    }

    @Override
    public List<Resource> toResources(Iterable<? extends Project> projects) {
        List<Resource> resources = new ArrayList<Resource>();
        for(Project project : projects) {
            resources.add(new Resource<Project>(project, linkTo(ProjectController.class).slash(project.getId()).withSelfRel()));
        }
        return resources;
    }

    @Override
    public Resource toResource(Project project) {
        return new Resource<Project>(project, linkTo(ProjectController.class).slash(project.getId()).withSelfRel());
    }





}
