package az.binar.web.rest.controllers;

import az.binar.web.core.model.Project;
import az.binar.web.core.service.ProjectService;
import az.binar.web.rest.controllers.asm.ProjectResourceAssembler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedResources;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by togrul on 12/5/14.
 */
@RestController
@RequestMapping("/projects")
public class ProjectController {

    @Autowired
    private ProjectService projectService;

    private ProjectResourceAssembler projectResourceAssembler;

    @RequestMapping("/")
    public PagedResources<Project> getAll(Pageable pageable, PagedResourcesAssembler assembler) {

        Page<Project> projects = projectService.getAllProjects(pageable);

        projectResourceAssembler = new ProjectResourceAssembler();

        return assembler.toResource(projects, projectResourceAssembler);

    }

}
