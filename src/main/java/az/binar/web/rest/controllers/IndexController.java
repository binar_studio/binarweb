package az.binar.web.rest.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by togrul on 11/30/14.
 */
@RestController
public class IndexController {

    @RequestMapping("/")
    public String index() {
        return "Response ok";
    }
}
