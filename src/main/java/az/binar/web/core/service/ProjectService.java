package az.binar.web.core.service;

import az.binar.web.core.model.Project;
import az.binar.web.core.repo.ProjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * Created by togrul on 12/5/14.
 */
@Service
public class ProjectService {

    @Autowired
    private ProjectRepository repository;

    public Page<Project> getAllProjects(Pageable pageable)
    {
        return repository.findAll(pageable);
    }

}
