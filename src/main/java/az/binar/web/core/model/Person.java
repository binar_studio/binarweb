package az.binar.web.core.model;

import javax.persistence.*;
import java.util.List;

/**
 * Created by togrul on 11/29/14.
 */

@Entity
@Inheritance(strategy= InheritanceType.JOINED)
@DiscriminatorColumn(name="TYPE", discriminatorType= DiscriminatorType.CHAR)
@DiscriminatorValue("P")
public class Person {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String firstName;
    private String lastName;
    private String company;

    @OneToMany(mappedBy = "person")
    private List<Profile> profiles;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public List<Profile> getProfiles() {
        return profiles;
    }

    public void setProfiles(List<Profile> profiles) {
        this.profiles = profiles;
    }
}
