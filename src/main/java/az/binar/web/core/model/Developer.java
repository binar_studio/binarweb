package az.binar.web.core.model;

import javax.persistence.*;
import java.util.List;

/**
 * Created by togrul on 11/29/14.
 */

@Entity
@DiscriminatorValue("D")
public class Developer extends Profile {

    @ManyToMany
    @JoinTable(name="DEVELOPERS_PROJECTS")
    private List<Project> projects;
    @ManyToMany
    @JoinTable(name="DEVELOPERS_SKILLS")
    private List<Skill> skills;

    public List<Project> getProjects() {
        return projects;
    }

    public void setProjects(List<Project> projects) {
        this.projects = projects;
    }

    public List<Skill> getSkills() {
        return skills;
    }

    public void setSkills(List<Skill> skills) {
        this.skills = skills;
    }
}
