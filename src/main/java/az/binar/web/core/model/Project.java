package az.binar.web.core.model;

import javax.persistence.*;
import java.util.List;

/**
 * Created by togrul on 11/28/14.
 */

@Entity
public class Project {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    @ManyToOne()
    @JoinColumn(name="CUSTOMER_ID", nullable=false)
    private Customer customer;
    @ManyToMany(mappedBy="projects")
    private List<Developer> developers;
    @ManyToOne
    @JoinColumn(name="MANAGER_ID", nullable=false)
    private Manager manager;
    @Basic
    private Boolean completed = false;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public List<Developer> getDevelopers() {
        return developers;
    }

    public void setDevelopers(List<Developer> developers) {
        this.developers = developers;
    }

    public Manager getManager() {
        return manager;
    }

    public void setManager(Manager manager) {
        this.manager = manager;
    }

    public Boolean getCompleted() {
        return completed;
    }

    public void setCompleted(Boolean completed) {
        this.completed = completed;
    }
}
