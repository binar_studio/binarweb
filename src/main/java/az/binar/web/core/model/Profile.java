package az.binar.web.core.model;

import javax.persistence.*;

/**
 * Created by togrul on 11/29/14.
 */

@Entity
@Inheritance(strategy= InheritanceType.JOINED)
@DiscriminatorColumn(name="TYPE", discriminatorType= DiscriminatorType.CHAR)
@DiscriminatorValue("P")
public class Profile {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @ManyToOne
    @JoinColumn(name="PERSON_ID", nullable=false)
    private Person person;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }
}
