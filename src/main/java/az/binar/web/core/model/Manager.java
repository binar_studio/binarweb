package az.binar.web.core.model;

import javax.persistence.*;
import java.util.List;

/**
 * Created by togrul on 11/29/14.
 */

@Entity
@DiscriminatorValue("M")
public class Manager extends Profile {

    @OneToMany(cascade= CascadeType.ALL, mappedBy="manager")
    private List<Project> projects;

    public List<Project> getProjects() {
        return projects;
    }

    public void setProjects(List<Project> projects) {
        this.projects = projects;
    }
}
