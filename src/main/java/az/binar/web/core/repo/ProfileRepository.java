package az.binar.web.core.repo;

import az.binar.web.core.model.Profile;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by togrul on 12/1/14.
 */
@Repository
public interface ProfileRepository extends PagingAndSortingRepository<Profile, Long> {


}
