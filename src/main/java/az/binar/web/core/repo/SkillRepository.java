package az.binar.web.core.repo;

import az.binar.web.core.model.Skill;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by togrul on 12/1/14.
 */
@Repository
public interface SkillRepository extends PagingAndSortingRepository<Skill, Long> {


}
