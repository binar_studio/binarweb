package az.binar.web.core.repo;

import az.binar.web.core.model.Customer;
import az.binar.web.core.model.Manager;
import az.binar.web.core.model.Project;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by togrul on 12/1/14.
 */
@Repository
public interface ProjectRepository extends PagingAndSortingRepository<Project, Long> {

    List<Project> findByCompletedTrue();

    List<Project> findByCompletedFalse();

    List<Project> findByCustomer(Customer customer);

    List<Project> findByManager(Manager manager);

}
