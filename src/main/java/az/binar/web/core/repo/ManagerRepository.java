package az.binar.web.core.repo;

import az.binar.web.core.model.Manager;
import az.binar.web.core.model.Project;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by togrul on 12/1/14.
 */
@Repository
public interface ManagerRepository extends PagingAndSortingRepository<Manager, Long> {

}
