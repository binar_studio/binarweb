package az.binar.web.core.repo;

import az.binar.web.Application;
import az.binar.web.core.model.Person;
import az.binar.web.core.repo.PersonRepository;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
public class RepositoriesTests {

	@Autowired
	protected PersonRepository personRepository;

	@Test
	@Rollback(false)
	public void createPersons() {

		Person p1 = new Person();
		p1.setFirstName("Test");
		p1.setLastName("Mest");

		personRepository.save(p1);

		Person p1Saved = personRepository.findOne(p1.getId());

		assertEquals(p1.getId(), p1Saved.getId());

	}

}
